class AddFaviconToAppConfigs < ActiveRecord::Migration[4.2]
  def up
    add_attachment :app_configs, :favicon
  end

  def down
    remove_attachment :app_configs, :favicon
  end
end